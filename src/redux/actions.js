
export function startLoadingRecords(search,sortdirection,sorttype) {
    const baseUrl="https://hur-pages-api.herokuapp.com/api/authorization/rights_and_roles_elements?"
    let url=baseUrl;
   
  console.log(search);
    if(search)url+=`search=${search}&`;
    if(sortdirection)
    url+=`sort_direction=${sortdirection}&sort_type=${sorttype}`;
   console.log(url);
     return dispatch => {
        
        fetch(url)
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
           
   
            let records=[];
                    res["included"].forEach((child) => {
                     records.push( {name:child["attributes"]["label"],art:child["type"]});
                    });
   
            dispatch(loadRecords(records)) 
                
        })
        .catch((err) => { throw(err) })
    }
}


export function loadRecords(records) {
 
    return {
        type:'LOAD_RECORDS',
        records
    }
}