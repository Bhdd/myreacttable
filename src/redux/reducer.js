import {combineReducers} from 'redux'
function records(state={}, action){

    switch(action.type){
      
        case 'LOAD_RECORDS':    return action.records;
        default:  return state;
    }
    
}

const rootReducer= combineReducers({  records })
export default rootReducer