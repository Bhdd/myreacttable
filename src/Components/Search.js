import React, { Component } from 'react';
import queryString from 'query-string'
class Search extends Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event) {
        event.preventDefault();

        const search = event.target.search.value;
        const sorttype = "label";
        var values = queryString.parse(this.props.location.search);

        let url = `/listing?`;
        let sortdirection = "desc";
        if (search !== '')
            url += `search=${search}&`;

        if (values.sort_direction) {sortdirection = values.sort_direction;
        url += `sort_direction=${sortdirection}&sort_type=${sorttype}`;
        }
        this.props.startLoadingRecords(search, sortdirection, sorttype);
        this.props.history.push(url);

    }
    render() {

        return (
            <div>

                <form onSubmit={this.handleSubmit}>

                    <input type="text"  name="search" className='mytext'></input>

                    <button type='submit' className='mybutton'>Search</button>
                </form>
            </div>)
    }

}
export default Search