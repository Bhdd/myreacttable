import Main from './Main'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../redux/actions';
import {withRouter} from 'react-router';
function mapActionToProps(dispatch){
    return bindActionCreators(actions,dispatch)
}

function mapStateToProps(state){
        return{records:state.records,sortdirection:state.sortdirection,sorttype:state.sorttype}
}
const App=withRouter(connect(mapStateToProps,mapActionToProps)(Main))

export default App;