import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Title from './Title'
import Search from './Search'
import RecordList from './RecordList'

class Main extends Component {

    componentDidMount() {

        this.props.startLoadingRecords();
    }


    render() {

        return (<div>

            <Route exact path="/" render={() => (
                <div>
                    <Title title={'List records'} />
                    <Search {...this.props} ></Search>
                    <RecordList  {...this.props} />
                </div>
            )} />
            <Route exact path="/listing" render={() => (
                <div>
                    <Title title={'List records'} />
                    <Search {...this.props} ></Search>
                    <RecordList  {...this.props} />
                </div>
            )} />

        </div>)
    }
}
export default Main