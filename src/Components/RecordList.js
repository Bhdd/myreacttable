import React, { Component } from 'react';
import queryString from 'query-string'
class RecordList extends Component {
    constructor() {
        super();
        this.handleSort = this.handleSort.bind(this);
    }
    handleSort(event) {
        event.preventDefault();
        const sorttype = "label";
        var values = queryString.parse(this.props.location.search);

        let url = `/listing?`;
        let sortdirection = "desc";
        if (values.search) url += `search=${values.search}&`;

        if (values.sort_direction) sortdirection = values.sort_direction === "desc" ? "asc" : "desc";
        url += `sort_direction=${sortdirection}&sort_type=${sorttype}`;

        this.props.startLoadingRecords(values.search, sortdirection, sorttype);
        this.props.history.push(url);
    }

    render() {


        return <div className="">

            <table className='table'>
                <thead>
                    <tr>
                        <th ><button className='mybutton'  >Art</button></th>
                        <th ><button  className='mybutton'  onClick={this.handleSort}>Name</button></th>
                    </tr>
                </thead>
                <tbody>
                    {

                        Array.from(this.props.records).map((record, index) =>
                            <tr key={index}>
                                <td>{record.art}</td>
                                <td>{record.name}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>

        </div>

    }
}

export default RecordList